
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from PyQt5.QtWidgets import QSizePolicy
from matplotlib.figure import Figure
from numpy import sin
from numpy import pi
from numpy import arange
from time import sleep
from numpy import append

class Graph(FigureCanvasQTAgg):
	"""docstring width, height, dpi Graph"""
	def __init__(self, parent=None, width=5, height=4, dpi=100):
		self.t = arange(0.0,1,0.01)
		#Creamos un objeto tipo Figure
		self.__fig = Figure(figsize=(width, height), dpi=dpi)
		#Creamos un lienzo sobre el cual dibujar nuestra gráfica
		self.axes = self.__fig.add_subplot(111)
		#Inicializamos la clase FigureCanvasQTAgg
		FigureCanvasQTAgg.__init__(self, self.__fig)
		self.setParent(parent)
		FigureCanvasQTAgg.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
		FigureCanvasQTAgg.updateGeometry(self)

	def compute_initial_figure(self):
		self.t = append(self.t, self.t[-1]+0.01)
		s = sin(2*pi*self.t)
		self.axes.cla()	
		self.axes.plot(self.t[-1000:],s[-1000:])
		self.__fig.canvas.draw()




