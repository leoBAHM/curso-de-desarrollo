import sys
sys.path.append("..")
from PyQt5.QtWidgets import QMainWindow, QVBoxLayout, QSizePolicy 
from PyQt5.QtCore import QTimer
from PyQt5.uic import loadUi
from numpy import arange

class MyWindow(QMainWindow):
	"""docstring for MyWindow"""
	def __init__(self):
		QMainWindow.__init__(self)
		loadUi("../Vista/Interfaces/interfaz.ui", self)


	def setup(self):
		self.area_grafica.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		self.area_grafica.updateGeometry()
		layout = QVBoxLayout()
		self.area_grafica.setLayout(layout)
		layout.addWidget(self.__control.get_model())

		self.btn_graficar.clicked.connect(self.adquirir)

	def adquirir(self):
		timer = QTimer(self)
		timer.timeout.connect(self.__control.set_graph)
		timer.start(1)

	def my_control(self, control):
		self.__control = control



