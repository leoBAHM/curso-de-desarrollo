
import sys
sys.path.append("..")
from PyQt5.QtWidgets import QApplication
from Vista.vista import MyWindow
from Modelo.modelo import Graph

class MyControl(object):
	"""docstring for MyControl"""
	def __init__(self):
		self.app = QApplication(sys.argv)
		self.__my_window = MyWindow()
		self.__my_model = Graph()
		self.__my_control = Controlador(self.__my_window, self.__my_model)
		self.__my_window.my_control(self.__my_control)
		self.__my_window.setup()

	def setup(self):
		self.__my_window.show()
		sys.exit(self.app.exec_())

class Controlador(object):
	def __init__(self, vista, modelo):
		self.__my_view = vista
		self.__my_model = modelo

	def get_model(self):
		return self.__my_model

	def set_graph(self):
		self.__my_model.compute_initial_figure()

if __name__ == "__main__":
	x = MyControl()
	x.setup()		